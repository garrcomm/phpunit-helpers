<?php

use Garrcomm\PHPUnitHelpers\Mysqli\MysqliMock;

/**
 * Sets mysqli error reporting mode
 *
 * @param integer $flags Depending on the flags, it sets mysqli error reporting mode to exception, warning or none.
 *
 * @return boolean
 */
function mysqli_report(int $flags): bool
{
    MysqliMock::setTestValue('mysqli_report', $flags);
    return true;
}

/**
 * Escapes special characters for use in an SQL statement, taking into account the current charset of the connection
 *
 * @param mysqli $mysql  A mysqli object returned by mysqli_connect() or mysqli_init().
 * @param string $string The string to be escaped.
 *
 * @return string
 */
function mysqli_real_escape_string(mysqli $mysql, string $string): string
{
    return $mysql->real_escape_string($string);
}
