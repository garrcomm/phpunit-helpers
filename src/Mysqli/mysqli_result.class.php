<?php

use Garrcomm\PHPUnitHelpers\MockedClass;

/**
 *  Represents the result set obtained from a query against the database.
 */
class mysqli_result extends MockedClass implements IteratorAggregate
{
    /**
     * Storage for the dataset
     *
     * @var array
     */
    protected $dataSet = array();

    /**
     * This method constructs a new mysqli_result object.
     */
    public function __construct()
    {
        throw new \RuntimeException('Use mysqli_result::mockFromArray() instead');
    }

    /**
     * Fetch the next row of a result set as an associative array
     *
     * @return array|null|false
     */
    public function fetch_assoc()
    {
        $return = current($this->dataSet);
        next($this->dataSet);
        return $return;
    }

    /**
     * Fetch the next row of a result set as an enumerated array
     *
     * @return array|null|false
     */
    public function fetch_row()
    {
        return array_values($this->fetch_assoc());
    }

    /**
     * Creates a mocked mysqli_result based on a multidimensional associative array
     *
     * @param array $dataSet The dataset.
     *
     * @return static
     */
    public static function mockFromArray(array $dataSet): self
    {
        // We use a serialized value so the constructor will be ignored
        $serialized = "O:" . strlen(__CLASS__) . ":\"" . __CLASS__ . "\":1:{s:10:\"\0*\0dataSet\";";
        $serialized .= serialize($dataSet) . '}';

        return unserialize($serialized);
    }

    /**
     * Frees the memory associated with a result
     *
     * @return void
     */
    public function free(): void
    {
        $this->dataSet = array();
    }

    /**
     * Retrieve an external iterator
     *
     * @return Iterator|void
     */
    public function getIterator()
    {
        throw new \RuntimeException('Method "' . get_called_class() . '->getIterator()" called which is not mocked');
    }
}
