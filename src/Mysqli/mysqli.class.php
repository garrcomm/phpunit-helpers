<?php

use Garrcomm\PHPUnitHelpers\Mysqli\MysqliMock;
use Garrcomm\PHPUnitHelpers\MockedClass;

/**
 * Mocked version of the mysqli class
 */
class mysqli extends MockedClass
{
    /**
     * Opens a new connection to the MySQL server
     *
     * @param string|null  $hostname MySQL hostname.
     * @param string|null  $username MySQL username.
     * @param string|null  $password MySQL password.
     * @param string|null  $database MySQL database.
     * @param integer|null $port     MySQL port number.
     * @param string|null  $socket   MySQL socket path.
     */
    public function __construct(
        string $hostname = null,
        string $username = null,
        string $password = null,
        string $database = null,
        int $port = null,
        string $socket = null
    ) {
        MysqliMock::setTestValue('__construct', func_get_args());
    }

    /**
     * Performs a query on the database
     *
     * @param string  $query       The query string.
     * @param integer $result_mode One of 3 constants indicating how the result will be returned from the MySQL server.
     *
     * @return mysqli_result|bool
     */
    public function query(string $query, int $result_mode = MYSQLI_STORE_RESULT)
    {
        $callable = MysqliMock::getTestValue('queryResponder');
        if (!$callable) {
            throw new \RuntimeException('No query responder configured. Call MysqliMock::setQueryResponder first');
        }
        $result = call_user_func_array($callable, func_get_args());
        if ($result === null) {
            throw new \RuntimeException('Query "' . $query . '" not mocked');
        }
        return $result;
    }

    /**
     * Prepares an SQL statement for execution.
     *
     * @param string $query The query, as a string. It must consist of a single SQL statement.
     *
     * @return mysqli_stmt
     */
    public function prepare(string $query): mysqli_stmt
    {
        return new mysqli_stmt($this, $query);
    }

    /**
     * Escapes special characters for use in an SQL statement, taking into account the current charset of the connection
     *
     * @param string $string The string to be escaped.
     *
     * @return string
     */
    public function real_escape_string(string $string): string
    {
        return str_replace(["\0", "\n", "\r", "'", '"'], ['\0', '\n', '\r', "\\'", '\\"'], $string);
    }
}
