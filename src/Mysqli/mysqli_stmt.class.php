<?php

use Garrcomm\PHPUnitHelpers\MockedClass;

/**
 * Mysqli prepared statement
 *
 * @property-read int param_count
 */
class mysqli_stmt extends MockedClass
{
    /**
     * Reference to the mysqli class
     *
     * @var mysqli
     */
    private $mysqli;
    /**
     * SQL query
     *
     * @var string|null
     */
    private $query;
    /**
     * Param types
     *
     * @var string
     */
    private $paramTypes;
    /**
     * List of parameters
     *
     * @var array
     */
    private $params = array();

    /**
     * Reference to the most recent mysqli_result
     *
     * @var mysqli_result|null
     */
    private $result;

    /**
     * Constructs a new mysqli_stmt object
     *
     * @param mysqli      $mysql A valid mysqli object.
     * @param string|null $query The query, as string.
     */
    public function __construct(mysqli $mysql, ?string $query = null)
    {
        $this->mysqli = $mysql;
        $this->query = $query;
    }

    /**
     * Bind variables for the parameter markers in the SQL statement prepared.
     *
     * @param string $types   A string that contains characters which specify the types for the bind variables (idsb).
     * @param mixed  ...$vars The actual parameters to bind.
     *
     * @return boolean
     */
    public function bind_param(string $types, &...$vars): bool
    {
        // @TODO Validate input and simulate exception behaviour
        $this->paramTypes = $types;
        $this->params = &$vars;
        return true;
    }

    /**
     * Executes a prepared statement
     *
     * @param array|null $params An optional list of parameters.
     *
     * @return boolean
     */
    public function execute(?array $params = null): bool
    {
        if ($params === null) {
            // @TODO Is this behaviour correctly?
            $params = array_values($this->params);
        }

        // Include sanitized parameters
        $query = $this->query;
        if (strlen($this->paramTypes) > 0) {
            foreach (str_split($this->paramTypes) as $iterator => $paramType) {
                switch ($paramType) {
                    case 'i':
                        $sanitized = intval($params[$iterator]);
                        break;
                    case 'd':
                        $sanitized = floatval($params[$iterator]);
                        break;
                    case 's':
                    case 'b':
                        $sanitized = '\'' . $this->mysqli->real_escape_string($params[$iterator]) . '\'';
                        break;
                    default:
                        // @TODO Implement correct error
                        throw new \RuntimeException('Wrong paramType: ' . $paramType);
                }
                $query = preg_replace('/\?/', $sanitized, $query, 1);
            }
        }

        $this->result = $this->mysqli->query($query);

        return true;
    }

    /**
     * Gets a result set from a prepared statement as a mysqli_result object
     *
     * @return mysqli_result|false
     */
    public function get_result()
    {
        return $this->result ?? false;
    }

    /**
     * Magic getter handles read-only properties
     *
     * @param string $name Name of the property.
     *
     * @return mixed
     */
    public function __get(string $name)
    {
        if ($name == 'param_count') {
            return strlen($this->paramTypes);
        }
        return parent::__get($name);
    }
}
