<?php

/**
 * This is basically what the mysqli_sql_exception class in the mysqli extension does
 */
final class mysqli_sql_exception extends RuntimeException
{
    /**
     * The sql state with the error.
     *
     * @var string
     */
    protected $sqlstate;

    /**
     * Returns the SQLSTATE error code
     *
     * @return string
     */
    public function getSqlState(): string
    {
        return $this->sqlstate;
    }
}
