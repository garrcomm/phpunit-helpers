<?php

namespace Garrcomm\PHPUnitHelpers\Mysqli;

/**
 * Mysqli Mock helper
 */
class MysqliMock
{
    /**
     * Collection of test data. Should be reset before each test.
     *
     * @var array
     */
    private static $testData = array();

    /**
     * Set to true when we're initialized
     *
     * @var bool
     */
    private static $initialized = false;

    /**
     * Resets all test data, so we can safely execute another test. Should be called in the tearDown() method of a test.
     *
     * @return void
     */
    public static function reset(): void
    {
        static::$testData = array();
    }

    /**
     * Sets specific test values
     *
     * @param string $key   The key.
     * @param mixed  $value The value.
     *
     * @return void
     */
    public static function setTestValue(string $key, $value)
    {
        static::$testData[$key] = $value;
    }

    /**
     * Gets specific test values
     *
     * @param string $key The key.
     *
     * @return mixed
     */
    public static function getTestValue(string $key)
    {
        return static::$testData[$key];
    }

    /**
     * Tests the mysqli::__construct call
     *
     * @param callable $method Method that asserts all parameters. Will be called with the full __constructor() payload.
     *
     * @return void
     */
    public static function assertConstructor(callable $method): void
    {
        call_user_func_array($method, static::$testData['__construct']);
    }

    /**
     * Tests the mysqli_report() method call
     *
     * @param callable $method Method that asserts the flags.
     *
     * @return void
     */
    public static function assertMysqliReport(callable $method): void
    {
        call_user_func($method, static::$testData['mysqli_report']);
    }

    /**
     * Sets the query responder callable
     *
     * @param callable $method The callable.
     *
     * @return void
     */
    public static function setQueryResponder(callable $method): void
    {
        static::$testData['queryResponder'] = $method;
    }

    /**
     * Initializes the MysqliMock class
     *
     * @return void
     */
    public static function init(): void
    {
        if (static::$initialized) {
            return;
        }

        if (class_exists('mysqli')) {
            throw new \RuntimeException(
                'The mysqli extension is loaded. Try running PHP with the -n parameter so we can mock mysqli.'
                . ' If you\'re using xdebug for code coverage, also add the parameters '
                . '-d zend_extension=xdebug -d xdebug.mode=coverage -d extension=tokenizer'
            );
        }

        // These files should be loaded when the MysqliMock is in use, not before!
        require_once __DIR__ . '/defines.php';
        require_once __DIR__ . '/functions.php';
        require_once __DIR__ . '/mysqli.class.php';
        require_once __DIR__ . '/mysqli_stmt.class.php';
        require_once __DIR__ . '/mysqli_result.class.php';
        require_once __DIR__ . '/mysqli_sql_exception.class.php';

        // Always test safe
        register_shutdown_function(function () {
            if (count(static::$testData) > 0) {
                throw new \RuntimeException(
                    'Always clean up after your tests. Run ' . __CLASS__ . '::reset() in the '
                    . 'tearDown() method in your test. Refusing to do so can lead to unpredictable test results.'
                );
            }
        });

        static::$initialized = true;
    }
}
