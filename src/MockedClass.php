<?php

namespace Garrcomm\PHPUnitHelpers;

/**
 * Generic mocked class
 */
class MockedClass
{
    /**
     * Magic method implemented to prevent unexpected results
     *
     * @param string $name      Name of the called method.
     * @param array  $arguments Arguments added when the method was called.
     *
     * @return mixed
     */
    public function __call(string $name, array $arguments)
    {
        throw new \RuntimeException('Method "' . get_called_class() . '->' . $name . '()" called which is not mocked');
    }

    /**
     * Magic method implemented to prevent unexpected results
     *
     * @param string $name      Name of the called method.
     * @param array  $arguments Arguments added when the method was called.
     *
     * @return mixed
     */
    public static function __callStatic(string $name, array $arguments)
    {
        throw new \RuntimeException('Method "' . get_called_class() . '::' . $name . '()" called which is not mocked');
    }

    /**
     * Magic method implemented to prevent unexpected results
     *
     * @param string $name Name of the requested property name.
     *
     * @return mixed
     */
    public function __get(string $name)
    {
        throw new \RuntimeException(
            'Variable "' . get_called_class() . '->' . $name . '" requested which is not mocked'
        );
    }

    /**
     * Magic method implemented to prevent unexpected results
     *
     * @param string $name  Name of the defined property name.
     * @param mixed  $value Value for the defined property.
     *
     * @return void
     */
    public function __set(string $name, $value)
    {
        throw new \RuntimeException('Variable "' . get_called_class() . '->' . $name . '" set which is not mocked');
    }
}
