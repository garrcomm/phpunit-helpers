<?php

namespace Garrcomm\PHPUnitHelpers;

class FunctionMock
{
    /**
     * Callback registry
     *
     * @var callable[]
     */
    private static $mocks = array();

    /**
     * Initialization status
     *
     * @var bool
     */
    private static $initialized = false;

    /**
     * Mocks a specific function within a specific namespace
     *
     * @param string   $namespace    The namespace.
     * @param string   $functionName Name of the function.
     * @param callable $response     Method that should be called instead.
     *
     * @return void
     */
    public static function mock(string $namespace, string $functionName, callable $response): void
    {
        // Initializes (if we're not already initialized)
        static::init();

        // Registers the callback
        static::$mocks[$namespace][$functionName] = $response;

        if (!function_exists($namespace . '\\' . $functionName)) {
            $phpCode = 'namespace ' . $namespace . ';' . PHP_EOL
                . 'function ' . $functionName . '() {' . PHP_EOL
                . 'return \\' .  __CLASS__ . '::mockCalled("' . $namespace . '", "' . $functionName
                . '", func_get_args());' . PHP_EOL
                . '}' . PHP_EOL
            ;
            eval($phpCode);
        }
    }

    /**
     * Releases all mocked functions
     *
     * @return void
     */
    public static function releaseAll(): void
    {
        static::$mocks = array();
    }

    /**
     * Releases a mocked function
     *
     * @param string $namespace    The namespace.
     * @param string $functionName Name of the function.
     *
     * @return void
     */
    public static function releaseMock(string $namespace, string $functionName): void
    {
        unset(static::$mocks[$namespace][$functionName]);
    }

    /**
     * When a mocked function is called, this method locates and executes the callback
     *
     * @param string $namespace    The namespace.
     * @param string $functionName Name of the function.
     * @param array  $arguments    Array with all arguments.
     *
     * @return   mixed
     * @internal
     */
    public static function mockCalled(string $namespace, string $functionName, array $arguments)
    {
        if (isset(static::$mocks[$namespace][$functionName])) {
            $function = static::$mocks[$namespace][$functionName];
        } else {
            $function = '\\' . $functionName;
        }
        return call_user_func_array($function, $arguments);
    }

    /**
     * Initializes the FunctionMock
     *
     * @return void
     */
    private static function init(): void
    {
        if (static::$initialized) {
            return;
        }
        register_shutdown_function(function () {
            foreach (static::$mocks as $namespace) {
                foreach ($namespace as $method) {
                    throw new \RuntimeException(
                        'Always clean up after your tests. Run ' . __CLASS__ . '::releaseAll() in the '
                        . 'tearDown() method in your test. Refusing to do so can lead to unpredictable test results.'
                    );
                }
            }
        });
        static::$initialized = true;
    }
}
