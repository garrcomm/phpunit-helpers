<?php

namespace App\Tests;

use Garrcomm\PHPUnitHelpers\FunctionMock;
use PHPUnit\Framework\TestCase;

class FunctionMockTest extends TestCase
{
    /**
     * This method is called after each test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        FunctionMock::releaseAll();
        parent::tearDown();
    }

    /**
     * Tests the FunctionMock
     *
     * @return void
     */
    public function testMock(): void
    {
        FunctionMock::mock(
            __NAMESPACE__,
            'mail',
            function ($to, $subject, $body) use (&$verify) {
                $verify = [
                    'to' => $to,
                    'subject' => $subject,
                    'body' => $body,
                ];
                return true;
            }
        );

        $result = mail('foo@bar.baz', 'subject', 'body');

        $this->assertTrue($result);
        $this->assertEquals('foo@bar.baz', $verify['to']);
        $this->assertEquals('subject', $verify['subject']);
        $this->assertEquals('body', $verify['body']);
    }
}
